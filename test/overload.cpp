#include "gtest/gtest.h"

struct C {};

template <typename O>
O& operator<<(O& o, C) {
  return o;
}

bool operator==(C, C) {
  return true;
}

TEST(PrintTest, Bug) {
  C x, y;
  EXPECT_EQ(x, y);
}
