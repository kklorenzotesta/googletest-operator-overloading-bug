# Dependencies

- [Bazel](https://bazel.build)
- [GCC](https://gcc.gnu.org)

# To reproduce
- run `bazel test //test:overload`

# Error message:
```
INFO: Analyzed target //test:overload (0 packages loaded, 0 targets configured).
INFO: Found 1 test target...
ERROR: /mnt/c/Users/l.testa/Desktop/tmpInterface/bugrepro/test/BUILD:1:8: Compiling test/overload.cpp failed: (Exit 1): gcc failed: error executing command
  (cd /home/ltesta/.cache/bazel/_bazel_ltesta/58869818396a88db4bae6e1159a5af58/sandbox/linux-sandbox/58/execroot/__main__ && \
  exec env - \
    PATH='/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/mnt/c/Program Files/WindowsApps/Microsoft.WindowsTerminal_1.7.1033.0_x64__8wekyb3d8bbwe:/mnt/c/Program Files (x86)/SEGGER/JLink:/mnt/c/Program Files/AdoptOpenJDK/jdk-11.0.9.101-hotspot/bin:/mnt/c/windows/system32:/mnt/c/windows:/mnt/c/windows/System32/Wbem:/mnt/c/windows/System32/WindowsPowerShell/v1.0/:/mnt/c/windows/System32/OpenSSH/:/mnt/c/Program Files (x86)/Nordic Semiconductor/nrf-command-line-tools/bin/:/mnt/c/Program Files (x86)/Nordic Semiconductor/nrf-command-line-tools/bin:/mnt/c/Program Files/PuTTY/:/mnt/c/Program Files/Git/cmd:/mnt/c/Program Files/CMake/bin:/mnt/c/Program Files (x86)/mingw-w64/i686-8.1.0-posix-dwarf-rt_v6-rev0/mingw32/bin:/mnt/c/Users/l.testa/AppData/Local/Android/Sdk/platform-tools:/mnt/c/Program Files (x86)/sbt/bin:/mnt/c/Program Files/Docker/Docker/resources/bin:/mnt/c/ProgramData/DockerDesktop/version-bin:/mnt/c/Users/l.testa/AppData/Local/Microsoft/WindowsApps:/mnt/c/Users/l.testa/AppData/Local/Programs/Microsoft VS Code/bin:/snap/bin:/mnt/c/Users/l.testa/Desktop/toolchain/gcc-arm-none-eabi-9-2020-q2-update/bin' \
    PWD=/proc/self/cwd \
  /usr/bin/gcc -U_FORTIFY_SOURCE -fstack-protector -Wall -Wunused-but-set-parameter -Wno-free-nonheap-object -fno-omit-frame-pointer '-std=c++0x' -MD -MF bazel-out/k8-fastbuild/bin/test/_objs/overload/overload.pic.d '-frandom-seed=bazel-out/k8-fastbuild/bin/test/_objs/overload/overload.pic.o' -fPIC -iquote . -iquote bazel-out/k8-fastbuild/bin -iquote external/gtest -iquote bazel-out/k8-fastbuild/bin/external/gtest -iquote external/bazel_tools -iquote bazel-out/k8-fastbuild/bin/external/bazel_tools '--std=c++14' -pipe '--std=c++14' -Iexternal/gtest/googletest/include/ -fno-canonical-system-headers -Wno-builtin-macro-redefined '-D__DATE__="redacted"' '-D__TIMESTAMP__="redacted"' '-D__TIME__="redacted"' -c test/overload.cpp -o bazel-out/k8-fastbuild/bin/test/_objs/overload/overload.pic.o)
Execution platform: @local_config_platform//:host

Use --sandbox_debug to see verbose messages from the sandbox gcc failed: error executing command
  (cd /home/ltesta/.cache/bazel/_bazel_ltesta/58869818396a88db4bae6e1159a5af58/sandbox/linux-sandbox/58/execroot/__main__ && \
  exec env - \
    PATH='/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/mnt/c/Program Files/WindowsApps/Microsoft.WindowsTerminal_1.7.1033.0_x64__8wekyb3d8bbwe:/mnt/c/Program Files (x86)/SEGGER/JLink:/mnt/c/Program Files/AdoptOpenJDK/jdk-11.0.9.101-hotspot/bin:/mnt/c/windows/system32:/mnt/c/windows:/mnt/c/windows/System32/Wbem:/mnt/c/windows/System32/WindowsPowerShell/v1.0/:/mnt/c/windows/System32/OpenSSH/:/mnt/c/Program Files (x86)/Nordic Semiconductor/nrf-command-line-tools/bin/:/mnt/c/Program Files (x86)/Nordic Semiconductor/nrf-command-line-tools/bin:/mnt/c/Program Files/PuTTY/:/mnt/c/Program Files/Git/cmd:/mnt/c/Program Files/CMake/bin:/mnt/c/Program Files (x86)/mingw-w64/i686-8.1.0-posix-dwarf-rt_v6-rev0/mingw32/bin:/mnt/c/Users/l.testa/AppData/Local/Android/Sdk/platform-tools:/mnt/c/Program Files (x86)/sbt/bin:/mnt/c/Program Files/Docker/Docker/resources/bin:/mnt/c/ProgramData/DockerDesktop/version-bin:/mnt/c/Users/l.testa/AppData/Local/Microsoft/WindowsApps:/mnt/c/Users/l.testa/AppData/Local/Programs/Microsoft VS Code/bin:/snap/bin:/mnt/c/Users/l.testa/Desktop/toolchain/gcc-arm-none-eabi-9-2020-q2-update/bin' \
    PWD=/proc/self/cwd \
  /usr/bin/gcc -U_FORTIFY_SOURCE -fstack-protector -Wall -Wunused-but-set-parameter -Wno-free-nonheap-object -fno-omit-frame-pointer '-std=c++0x' -MD -MF bazel-out/k8-fastbuild/bin/test/_objs/overload/overload.pic.d '-frandom-seed=bazel-out/k8-fastbuild/bin/test/_objs/overload/overload.pic.o' -fPIC -iquote . -iquote bazel-out/k8-fastbuild/bin -iquote external/gtest -iquote bazel-out/k8-fastbuild/bin/external/gtest -iquote external/bazel_tools -iquote bazel-out/k8-fastbuild/bin/external/bazel_tools '--std=c++14' -pipe '--std=c++14' -Iexternal/gtest/googletest/include/ -fno-canonical-system-headers -Wno-builtin-macro-redefined '-D__DATE__="redacted"' '-D__TIMESTAMP__="redacted"' '-D__TIME__="redacted"' -c test/overload.cpp -o bazel-out/k8-fastbuild/bin/test/_objs/overload/overload.pic.o)
Execution platform: @local_config_platform//:host

Use --sandbox_debug to see verbose messages from the sandbox
In file included from external/gtest/googletest/include/gtest/gtest-matchers.h:47,
                 from external/gtest/googletest/include/gtest/internal/gtest-death-test-internal.h:39,
                 from external/gtest/googletest/include/gtest/gtest-death-test.h:41,
                 from external/gtest/googletest/include/gtest/gtest.h:64,
                 from test/overload.cpp:1:
external/gtest/googletest/include/gtest/gtest-printers.h: In instantiation of 'void testing_internal::DefaultPrintNonContainerTo(const T&, std::ostream*) [with T = C; std::ostream = std::basic_ostream<char>]':
external/gtest/googletest/include/gtest/gtest-printers.h:468:49:   required from 'void testing::internal::DefaultPrintTo(testing::internal::WrapPrinterType<testing::internal::kPrintOther>, const T&, std::ostream*) [with T = C; std::ostream = std::basic_ostream<char>]'
external/gtest/googletest/include/gtest/gtest-printers.h:503:17:   required from 'void testing::internal::PrintTo(const T&, std::ostream*) [with T = C; std::ostream = std::basic_ostream<char>]'
external/gtest/googletest/include/gtest/gtest-printers.h:679:12:   required from 'static void testing::internal::UniversalPrinter<T>::Print(const T&, std::ostream*) [with T = C; std::ostream = std::basic_ostream<char>]'
external/gtest/googletest/include/gtest/gtest-printers.h:869:30:   required from 'void testing::internal::UniversalPrint(const T&, std::ostream*) [with T = C; std::ostream = std::basic_ostream<char>]'
external/gtest/googletest/include/gtest/gtest-printers.h:797:19:   [ skipping 2 instantiation contexts, use -ftemplate-backtrace-limit=0 to disable ]
external/gtest/googletest/include/gtest/gtest-printers.h:314:36:   required from 'static std::string testing::internal::FormatForComparison<ToPrint, OtherOperand>::Format(const ToPrint&) [with ToPrint = C; OtherOperand = C; std::string = std::__cxx11::basic_string<char>]'
external/gtest/googletest/include/gtest/gtest-printers.h:379:45:   required from 'std::string testing::internal::FormatForComparisonFailureMessage(const T1&, const T2&) [with T1 = C; T2 = C; std::string = std::__cxx11::basic_string<char>]'
external/gtest/googletest/include/gtest/gtest.h:1509:53:   required from 'testing::AssertionResult testing::internal::CmpHelperEQFailure(const char*, const char*, const T1&, const T2&) [with T1 = C; T2 = C]'
external/gtest/googletest/include/gtest/gtest.h:1531:28:   required from 'testing::AssertionResult testing::internal::CmpHelperEQ(const char*, const char*, const T1&, const T2&) [with T1 = C; T2 = C]'
external/gtest/googletest/include/gtest/gtest.h:1554:23:   required from 'static testing::AssertionResult testing::internal::EqHelper::Compare(const char*, const char*, const T1&, const T2&) [with T1 = C; T2 = C; typename std::enable_if<((! std::is_integral<_Tp>::value) || (! std::is_pointer<_Dp>::value))>::type* <anonymous> = 0]'
test/overload.cpp:16:3:   required from here
external/gtest/googletest/include/gtest/gtest-printers.h:287:7: error: ambiguous overload for 'operator<<' (operand types are 'std::ostream' {aka 'std::basic_ostream<char>'} and 'const C')
  287 |   *os << value;
      |   ~~~~^~~~~~~~
external/gtest/googletest/include/gtest/gtest-printers.h:232:41: note: candidate: 'std::basic_ostream<_CharT, _Traits>& testing::internal2::operator<<(std::basic_ostream<_CharT, _Traits>&, const T&) [with Char = char; CharTraits = std::char_traits<char>; T = C]'
  232 | ::std::basic_ostream<Char, CharTraits>& operator<<(
      |                                         ^~~~~~~~
test/overload.cpp:6:4: note: candidate: 'O& operator<<(O&, C) [with O = std::basic_ostream<char>]'
    6 | O& operator<<(O& o, C) {
      |    ^~~~~~~~
Target //test:overload failed to build
INFO: Elapsed time: 1.026s, Critical Path: 0.81s
INFO: 2 processes: 2 internal.
FAILED: Build did NOT complete successfully
//test:overload                                                 FAILED TO BUILD

FAILED: Build did NOT complete successfully
```